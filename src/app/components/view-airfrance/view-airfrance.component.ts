import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AEROPORTS } from 'src/app/constants/aeroport.constant';
import { IFiltres } from 'src/app/models/filtres.model';
import { Passager } from 'src/app/models/passager.model';
import { Vol } from 'src/app/models/vol.model';
import { PassagerService } from 'src/app/services/passager.service';
import { VolService } from '../../services/vol.service';

@Component({
  selector: 'app-view-airfrance',
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnInit, OnDestroy {

  vols: Vol[] = [];
  private _subscription: Subscription = new Subscription();
  selectedVol!: Vol;
  passagers!: Passager[];
  type!: string;

  constructor(
    private _volService: VolService,
    private _passagerService: PassagerService,
    private _activatedRoute: ActivatedRoute) { }


  ngOnInit(): void {
    const subscription = this._activatedRoute.data.subscribe((data$) => {
      this.type = data$['type'] ? data$['type'] : 'decollages';
    })

    this._subscription.add(subscription);
  }

  /**
   * Réaction à la mise à jour des filtres
   * On souhaite récupérer les vols qui correspondent aux filtres passés en paramètre
   * en utilisant le service défini dans le constructeur
   * @param filtres récupérés depuis le composant enfant
   */
  onFiltresEvent(filtres: IFiltres): void {
    // TODO

    let { aeroport, debut, fin } = filtres;

    let icao = aeroport.icao;
    let secondsDebut = Math.floor(debut.getTime() / 1000);
    let secondFin = Math.floor(fin.getTime() / 1000);

    let subscription: Subscription;

    if (this.type == "decollage") {
      subscription = this._volService.getVolsDepart(icao, secondsDebut, secondFin).subscribe((value) => {
        this.vols = value;
      })
    } else {
      subscription = this._volService.getVolsArrivee(icao, secondsDebut, secondFin).subscribe((value) => {
        this.vols = value;
      })
    }

    this._subscription.add(subscription);

  }

  setSelectedVol(vol: Vol) {
    this.selectedVol = vol;

    const subscription: Subscription = this._passagerService.getPassagers(vol.icao).subscribe((value) => {
      this.passagers = value;
    });

    this._subscription.add(subscription);
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
