import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BAGAGE_ACCORDING_TO_CLASS } from 'src/app/constants/aeroport.constant';
import { BagageClasse } from 'src/app/models/bagageClasse.model';

@Directive({
  selector: '[appClasseBagages]'
})
export class ClasseBagagesDirective implements OnChanges {

  @Input() appClasseBagages!: BagageClasse;

  constructor(private el: ElementRef) { }



  ngOnChanges(changes: SimpleChanges): void {
    let { classeVol, nbBagagesSoute } = this.appClasseBagages;

    switch (classeVol) {
      case 'STANDARD':
        if (nbBagagesSoute > 1) {
          this.el.nativeElement.style.backgroundColor = 'red';
        };
        break;
      case 'BUSINESS':
        if (nbBagagesSoute > 2) {
          this.el.nativeElement.style.backgroundColor = 'red';
        };
        break;
      case 'PREMIUM':
        if (nbBagagesSoute > 3) {
          this.el.nativeElement.style.backgroundColor = 'red';
        };
        break;
    }
  }

}
